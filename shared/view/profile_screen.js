'use strict';
import React, {Component} from "react";
import {Text} from "react-native";

// Create SettingsScreen class. When 'Settings' item is clicked, it will navigate to this page
export class ProfileScreen extends Component {
  render() {
    return <Text> Profile Page </Text>
  }
}