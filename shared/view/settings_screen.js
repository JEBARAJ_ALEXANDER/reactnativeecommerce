'use strict';
import React, {Component} from "react";
import {Text, ToolbarAndroid, View, StyleSheet} from "react-native";

export class SettingsScreen extends Component {
  render() {
    return (
      <View
        style={styles.containerToolbar}>
        <ToolbarAndroid
          style={styles.toolbar}
          title="AwesomeApp"/>
        <Text> Settings Page </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerToolbar: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  toolbar: {
    backgroundColor: '#e9eaed',
    height: 56,
  }
});