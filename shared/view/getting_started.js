'use strict';
import React, {Component} from "react";
import {Image, StyleSheet, Text, View} from "react-native";
import {Button} from "react-native-elements";

export class GettingStartedScreen extends Component {
  static navigationOptions = { header: null};
  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: "white"
      }}>
        <Image
          style={{
            resizeMode: Image.resizeMode.contain,
          }}
        />
        <Text style={styles.title}>NYTVATTA, BLIXTSNABBT.</Text>
        <Text style={styles.description}>Vi hamtar din tvatt och kemtvatt,
          rengor och levererar tillbaka till din
          dorr inom 48 timmar</Text>
        <View
          style={styles.buttons}>
          <View style={{
            flex: 1,
          }}>
            <Button
              title="Sign Up"
              textStyle={{fontWeight: "400", color: "#45dde6", fontSize: 23}}
              buttonStyle={{
                backgroundColor: "#fff",
                borderColor: "#45dde6",
                borderWidth: 2,
                borderRadius: 50
              }}
              containerStyle={{marginTop: 20}}
            />
          </View>
          <View style={{
            flex: 1,
          }}>
            <Button
              title="Login"
              textStyle={{fontWeight: "400", fontSize: 23}}
              buttonStyle={{
                backgroundColor: "#45dde6",
                borderColor: "#45dde6",
                borderWidth: 2,
                borderRadius: 50
              }}
              containerStyle={{marginTop: 20}}
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontWeight: "500",
    textAlign: "center",
    fontSize: 24,
    marginTop: 32,
    color: "#333333",
    fontFamily: "Roboto"
  },
  description: {
    color: "#777777",
    marginTop: 16,
    fontWeight: "400",
    textAlign: "center",
    fontSize: 22,
    fontFamily: "Roboto"
  },
  buttons: {
    margin: 16,
    flex: 1,
    flexDirection: 'row',
    position: "absolute",
    bottom: 32
  }
});
