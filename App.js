'use strict';
import React, {Component} from "react";
//import {DrawerNavigator} from "react-navigation";
import {GettingStartedScreen, HomeScreen} from "./shared";
import {StackNavigator} from "react-navigation";

/*
 const RootNavigation = DrawerNavigator({
 Home: {screen: HomeScreen},
 Profile: {screen: ProfileScreen},
 Settings: {screen: SettingsScreen}
 });
 */

export default class App extends Component {
    render() {
        return <RootStack />
    }
}
const RootStack = StackNavigator(
    {
        GettingStarted: {
            screen: GettingStartedScreen,
        },
        Home: {
            screen: HomeScreen,
        },
    },
    {
        initialRouteName: 'GettingStarted'
    }
);
