'use strict';
import React, {Component} from "react";
import {Image, StyleSheet, Text, TextInput, View} from "react-native";
import {TextInputLayout} from "rn-textinputlayout";
import {Button, Divider} from "react-native-elements";

export class HomeScreen extends Component {
  static navigationOptions = {
    headerTitle: (
      <Image
        style={{
          flex: 1,
          alignSelf: "center",
          resizeMode: Image.resizeMode.contain,
        }}
      />
    )
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title_layout}>
          <Text style={styles.title}>För att kunna skapa din konta sa</Text>
          <Text style={styles.title}>behover vi skicka en</Text>
          <Text style={styles.title}>verifieringskod till din telefon </Text>
        </View>
        <Divider style={{backgroundColor: '#f2f2f2'}}/>
        <TextInputLayout
          hintColor={"#999999"}
          focusColor={"#45dde6"}
          style={styles.inputLayout}>
          <TextInput
            style={styles.textInput}
            placeholder={'Land'}
          />
        </TextInputLayout>
        <TextInputLayout
          hintColor={"#999999"}
          focusColor={"#45dde6"}
          style={styles.inputLayout}>
          <TextInput
            style={styles.textInput}
            placeholder={'Telefonnummer'}
          />
        </TextInputLayout>
        <View
          style={styles.send_button}>
          <Button
            title="Skicka"
            titleStyle={{fontWeight: "700"}}
            buttonStyle={{
              backgroundColor: "#45dde6",
              width: 150,
              height: 45,
              borderColor: "transparent",
              borderWidth: 0,
              borderRadius: 50
            }}
            containerStyle={{marginTop: 20}}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: "white"
  },
  title: {
    fontWeight: "300",
    textAlign: "center",
    fontSize: 26,
    color: "#555555",
    fontFamily: "Roboto"
  },
  title_layout: {
    backgroundColor: "white",
    marginTop: 48,
    marginBottom: 48
  },
  send_button: {
    flex: 1,
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  textInput: {
    fontWeight: "400",
    fontSize: 16,
    height: 40
  },
  inputLayout: {
    marginTop: 16,
    marginHorizontal: 36
  }
});
